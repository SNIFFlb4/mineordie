﻿using System;
using UnityEngine;

class HealthComponent
{
    public event Action<object, int , int> OnHealthChanged;
    public event Action<object> OnPersonDead;

    public GameObject Parent { get; private set; }

    private int _currentHp;

    public int MaxHP { get; private set; }
    public int Hp
    {
        get => _currentHp;
        private set
        {
            _currentHp = Mathf.Clamp(value, 0, MaxHP);

            if (_currentHp <= 0)
                OnPersonDead?.Invoke(this);
            else
                OnHealthChanged?.Invoke(this, Hp, MaxHP);
        }
    }

    public HealthComponent(int maxHp, GameObject parent = null)
    {
        Parent = parent;
        MaxHP = maxHp;
        Hp = MaxHP;
    }

    public void TakeDamage(int damage) => Hp -= damage;

    public void RecoveryHealth(int value) => Hp += value;
}