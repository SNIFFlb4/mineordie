﻿using UnityEngine;

public enum Tag
{
    Player,
    Enemy,
    Resource
}

class TagSetter : MonoBehaviour
{
    [SerializeField]
    private Tag _tag;

    private void Awake()
    {
        gameObject.tag = _tag.ToString();
    }
}
