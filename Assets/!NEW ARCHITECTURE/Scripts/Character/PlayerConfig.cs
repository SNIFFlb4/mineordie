﻿using UnityEngine;

[CreateAssetMenu(fileName = "Player Config", menuName = "NewConfigs/PlayerConfig")]
class PlayerConfig : ScriptableObject
{
    public float Speed;
    public int StartIronCount;
    public int StartOilCount;
}