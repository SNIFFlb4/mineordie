﻿using UnityEngine;

class PlayerMovement : IInitialized
{
    private Rigidbody _rigidbody;
    private float _moveSpeed;
    private Transform _transform;

    public bool IsInitialized { get; private set; }

    public PlayerMovement (Rigidbody rigidbody, Transform transform, float speed)
    {
        _moveSpeed = speed;
        _rigidbody = rigidbody;
        _transform = transform;
    }

    public void Init()
    {
        IsInitialized = true;
    }

    public void Move(Vector2 moveDirection)
    {
        Vector3 move = new Vector3(moveDirection.x, 0, moveDirection.y);
        if (move != Vector3.zero)
        {
            _transform.forward = move;
        }

        _rigidbody.velocity = move * Time.deltaTime * _moveSpeed;
    }
}