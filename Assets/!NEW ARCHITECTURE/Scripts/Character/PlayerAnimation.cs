﻿using UnityEngine;

public enum PlayerAnimState
{
    IDLE,
    Walk,
    TakeTurret,
    PutTurret,
    StartDrill,
    EndDrill
}

class PlayerAnimation : IInitialized
{
    private const string BOOL_IS_WALK = "isWalking";
    private const string BOOL_WITH_TURRET = "withWeapon";
    private const string BOOL_IS_DRILL = "isDrill";

    private Animator _animator;
    private InputController _input;
    private PlayerAnimState _currentState;

    public bool IsInitialized { get; private set; }

    public PlayerAnimation (Animator animator, InputController input)
    {
        _animator = animator;
        _input = input;
    }

    public void Init()
    {
        ChangeState(PlayerAnimState.IDLE);

        IsInitialized = true;
    }

    public void ChangeState(PlayerAnimState newState)
    {
        if (_currentState == newState)
            return;

        _currentState = newState;

        switch (_currentState)
        {
            case PlayerAnimState.IDLE:
                _animator.SetBool(BOOL_IS_WALK, false);
                break;
            case PlayerAnimState.Walk:
                _animator.SetBool(BOOL_IS_WALK, true);
                break;
            case PlayerAnimState.TakeTurret:
                _animator.SetBool(BOOL_WITH_TURRET, true);
                break;
            case PlayerAnimState.PutTurret:
                _animator.SetBool(BOOL_WITH_TURRET, false);
                break;
            case PlayerAnimState.StartDrill:
                _animator.SetBool(BOOL_IS_DRILL, true);
                CameraController.Instance.SetNoise(0.5f, 0.5f);
                break;
            case PlayerAnimState.EndDrill:
                _animator.SetBool(BOOL_IS_DRILL, false);
                CameraController.Instance.StopNoise();
                break;
            default:
                Debug.LogError("Unknown player state");
                break;
        }
    }

    public void Loop()
    {
        if (_input.GetInput() == Vector2.zero)
        {
            ChangeState(PlayerAnimState.IDLE);
        }
        else
            ChangeState(PlayerAnimState.Walk);
    }
}