﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
class Player : MonoBehaviour, IInitialized
{
    public PlayerConfig Config;
    public bool IsWeaponInHands => WeaponInHands != null;

    [SerializeField]
    private Transform _placeForWeapon;

    public Weapon WeaponInHands { get; private set; }

    //Get from Unity
    [Header("Unity Components")]
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private Animator _animator;

    //New components
    public CharacterResources PlayerResources { get; private set; }
    public PlayerMovement PlayerMove { get; private set; }
    public PlayerAnimation PlayerAnimation { get; private set; }
    public InputController PlayerInput { get; private set; }

    public bool IsInitialized { get; set; }

    public void Init()
    {
        PlayerMove = new PlayerMovement(_rigidbody, this.transform, Config.Speed);
        PlayerMove.Init();

        PlayerInput = new InputController();

        PlayerAnimation = new PlayerAnimation(_animator, PlayerInput);
        PlayerAnimation.Init();

        PlayerResources = new CharacterResources(Config.StartIronCount, Config.StartOilCount);
        PlayerResources.Init();

        IsInitialized = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (WeaponInHands)
            return;

        if (other.CompareTag(GlobalVars.IRON_TAG))
        {
            PlayerAnimation.ChangeState(PlayerAnimState.StartDrill);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (WeaponInHands)
            return;

        if (other.CompareTag(GlobalVars.IRON_TAG))
        {
            PlayerAnimation.ChangeState(PlayerAnimState.EndDrill);
        }
    }

    private void Update()
    {
        if (IsInitialized is false)
            return;

        PlayerAnimation.Loop();
    }

    private void FixedUpdate()
    {
        if (IsInitialized is false)
            return;

        PlayerMove.Move(PlayerInput.GetInput());
    }

    public void TakeWeapon(Weapon weapon)
    {
        PlayerAnimation.ChangeState(PlayerAnimState.TakeTurret);
        WeaponInHands = weapon;
        WeaponInHands.transform.SetParent(_placeForWeapon);
        WeaponInHands.transform.position = _placeForWeapon.position;
        WeaponInHands.transform.rotation = _placeForWeapon.rotation;
        WeaponInHands.TurnOff();
    }

    public Weapon GetWeapon()
    {
        PlayerAnimation.ChangeState(PlayerAnimState.PutTurret);
        Weapon tempLinkOnWeapon = WeaponInHands;
        WeaponInHands = null;

        return tempLinkOnWeapon;
    }
}