﻿using System.Collections.Generic;
using UnityEngine;

class VFXSystem : MonoBehaviour
{
    [SerializeField]
    private List<VFXComponent> _effects;

    public void PlayEffect(VFXType type)
    {
        var selectedEffects = _effects.FindAll(e => e.Type.Equals(type));

        if (selectedEffects.Count != 0)
        {
            foreach (var e in selectedEffects)
            {
                e.PlayEffect();
            }
        }
        else
            Debug.Log($"There is no VFX with type {type}");
    }

    public void StopEffect(VFXType type)
    {
        var selectedEffects = _effects.FindAll(e => e.Type.Equals(type));

        if (selectedEffects.Count != 0)
        {
            foreach (var e in selectedEffects)
            {
                e.StopEffect();
            }
        }
        else
            Debug.Log($"There is no VFX with type {type}");
    }
}
