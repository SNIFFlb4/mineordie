﻿using System.Collections.Generic;
using UnityEngine;

public enum VFXType
{
    None = 0,
    WeaponShot = 1,
    WeaponCreated = 2,
    WeaponReloaded = 3,
    WeaponUpgraded = 4,
    WeaponUtilize = 5,
    Explosion = 6,
    WeaponPutOnPlatform = 7,
    DoingOperationOnPlatform = 8,
    WickOnBombEnemy = 9
}

class VFXComponent : MonoBehaviour
{
    [SerializeField] private List<ParticleSystem> allParticles;

    [SerializeField] private bool _playOnAwake;
    [SerializeField] private VFXType _type;

    public VFXType Type => _type;

    private void Awake()
    {
        foreach (var e in allParticles)
        {
            e.playOnAwake = _playOnAwake;
        }
    }

    public void PlayEffect ()
    {
        if (_playOnAwake)
        {
            gameObject.SetActive(false);
            gameObject.SetActive(true);
        }
        else
        {
            foreach (var e in allParticles)
            {
                e.Play();
            }
        }
    }

    public void StopEffect ()
    {
        if (_playOnAwake)
            gameObject.SetActive(false);
        else
        {
            foreach (var e in allParticles)
            {
                e.Stop();
            }
        }
    }
}