﻿using UnityEngine;

//[RequireComponent(typeof(Rigidbody))]
class ConcreteTestBullet : Bullet
{
    //private Rigidbody _rigidbody;

    //private void Awake()
    //{
    //    _rigidbody = GetComponent<Rigidbody>();
    //}

    protected override void HitTarget(Enemy enemy)
    {
        enemy.Health.TakeDamage(bulletDamage);
        Destroy(this.gameObject);
    }

    protected override void Move()
    {
        _rigidbody.AddForce(transform.forward * bulletSpeed);
    }
}