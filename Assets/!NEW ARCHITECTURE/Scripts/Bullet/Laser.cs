﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Laser : Bullet
{
    private Vector3 _startScale;

    protected override void Awake()
    {
        base.Awake();
        _startScale = transform.localScale;
    }

    protected override void HitTarget(Enemy enemy = null)
    {
        enemy?.Health.TakeDamage(bulletDamage);
    }

    protected override void Move()
    {
        ChangeLen();
    }

    private void ChangeLen()
    {
        transform.localScale += new Vector3(0, 0, 3);
        transform.localPosition = new Vector3(0, 0, transform.localScale.z / 2);
    }

    public override void ResetBeforeBackToPool()
    {
        base.ResetBeforeBackToPool();
        transform.localScale = _startScale;
        transform.position = startPosition;
        transform.rotation = startRotation;
    }
}
