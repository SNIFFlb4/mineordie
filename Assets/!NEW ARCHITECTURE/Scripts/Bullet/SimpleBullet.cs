﻿class SimpleBullet : Bullet
{
    protected override void HitTarget(Enemy enemyHealth)
    {
        //Take damage from Config numbers to the selected Enemy
        base.HitTarget(enemyHealth);
    }

    protected override void Move()
    {
        //Simple move to forward direction
        base.Move();
    }
}