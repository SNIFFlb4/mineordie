﻿using UnityEngine;

class FreeseBullet : Bullet
{
    [Header("Class attribute")]
    [SerializeField] private float _actionTime;
    [SerializeField] private float _force;

    protected override void HitTarget(Enemy enemyHealth)
    {
        base.HitTarget(enemyHealth);
        enemyHealth.SetCondition(Enemy.EnemyCondition.freeze, _actionTime, _force);
    }
}