﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
abstract class Bullet : MonoBehaviour, IPoolObject
{
    [Header("Base")]
    [SerializeField] protected float bulletSpeed;
    [SerializeField] protected int bulletDamage;
    [SerializeField] protected float timeToDestroyBullet = 3f;

    protected Enemy _enemy;
    protected Rigidbody _rigidbody;

    protected Vector3 startPosition;
    protected Quaternion startRotation;
    protected Vector3 targetPosition;

    public event Action<IPoolObject> OnObjectNeededToDeactivate;

    protected virtual void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    public virtual void StartMove()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        StartCoroutine(ReturnToPoolCuro());
    }

    private IEnumerator ReturnToPoolCuro ()
    {
        yield return new WaitForSeconds(timeToDestroyBullet);
        OnObjectNeededToDeactivate?.Invoke(this);
    }

    protected virtual void FixedUpdate()
    {
        Move();
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out _enemy))
        {
            HitTarget(_enemy);
        }
        else
            HitTarget();
    }

    /// <summary>
    /// Calls in FixedUpdate
    /// </summary>
    protected virtual void Move()
    {
        _rigidbody.velocity = transform.forward * bulletSpeed;
    }

    /// <summary>
    /// Calls when bullet hit to target with Enemy Component
    /// </summary>
    /// <param name="enemyHealth"></param>
    protected virtual void HitTarget(Enemy enemy = null)
    {
        enemy?.Health.TakeDamage(bulletDamage);

        OnObjectNeededToDeactivate?.Invoke(this);
    }

    public virtual void ResetBeforeBackToPool()
    {
        gameObject.SetActive(false);
    }

    public void SetPositions(Vector3 startPosition, Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
        this.startPosition = startPosition;
    }
}