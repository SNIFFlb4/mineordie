﻿using UnityEngine;

class ElectroBullet : Bullet
{
    [Header("Class attribute")]
    [SerializeField] private int _countsOfTarget;
    [SerializeField] private int _lightningDamage;
    [SerializeField] private float _lightningMaxDistance;
    [SerializeField] LayerMask enemyLayer;

    protected override void HitTarget(Enemy enemy)
    {
        LightningAttack(enemy);
        base.HitTarget(enemy);
    }

    private void LightningAttack(Enemy curEn)
    {
        int damaged = 0;

        var enemyColliders = Physics.OverlapSphere(transform.position, _lightningMaxDistance, enemyLayer);
        for (int i = 0; i < enemyColliders.Length; i++)
        {
            if (damaged > _countsOfTarget)
            {
                return;
            }

            if(TryGetComponent(out Enemy enemy))
            {
                if (curEn.Equals(enemy))
                    continue;

                enemy.Health.TakeDamage(_lightningDamage);

                damaged++;
            }
        }
    }
}