﻿using UnityEngine;
using System;

class CannonBallBullet : Bullet
{
    [Header("Class attribute")]
    [SerializeField, Range(0, 1)] private float _arcHeight = 0.25f;
    [SerializeField] private float _destroyRange;
    [SerializeField] private LayerMask _enemyLayer;
    [SerializeField] private VFXComponent explosionEffect;

    private float _stepScale;
    private float _progress;

    public override void StartMove()
    {
        base.StartMove();
        float distance = Vector3.Distance(startPosition, targetPosition);
        _arcHeight = _arcHeight * distance;

        // This is one divided by the total flight duration, to help convert it to 0-1 progress.
        _stepScale = bulletSpeed / distance;
    }

    protected override void Move()
    {
        // I presume you disable/destroy the arrow in Arrived so it doesn't keep arriving.
        if (_progress >= 1.0f)
        {
            //print(transform.forward);z r 
            //_rigidbody.velocity = transform.forward * bulletSpeed;
            return;
        }

        // Increment our progress from 0 at the start, to 1 when we arrive.
        _progress = Mathf.Min(_progress + Time.deltaTime * _stepScale, 1.0f);

        // Turn this 0-1 value into a parabola that goes from 0 to 1, then back to 0.
        float parabola = 1.0f - 4.0f * (_progress - 0.5f) * (_progress - 0.5f);

        // Travel in a straight line from our start position to the target.        
        Vector3 nextPos = Vector3.Lerp(startPosition, targetPosition, _progress);

        // Then add a vertical arc in excess of this.
        nextPos.y += parabola * _arcHeight;

        // Continue as before.
        transform.LookAt(nextPos, transform.forward);
        transform.position = nextPos;
    }

    protected override void HitTarget(Enemy enemyInTarget)
    {
        var explosion = Instantiate(explosionEffect, transform.position, transform.rotation);
        Destroy(explosion.gameObject, 2f);

        var enemyInRange = Physics.OverlapSphere(transform.position, _destroyRange, _enemyLayer);
        for (int i = 0; i < enemyInRange.Length; i++)
        {
            if (enemyInRange[i] == null)
                continue;

            if (enemyInRange[i].TryGetComponent(out Enemy enemy))
            {
                float distance = Vector3.Distance(transform.position, enemy.transform.position);
                if (distance < 1)
                    distance = 1;
                int damage = Convert.ToInt32(bulletDamage / distance);
                enemy.Health.TakeDamage(damage);
            }
        }
    }

    public override void ResetBeforeBackToPool()
    {
        base.ResetBeforeBackToPool();
        _progress = 0;
        _arcHeight = 0.25f;
    }
}