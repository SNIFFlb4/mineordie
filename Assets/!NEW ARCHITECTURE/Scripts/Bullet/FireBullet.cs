﻿using UnityEngine;

class FireBullet : Bullet
{
    [Header("Class attribute")]
    [SerializeField] private float _actionTime;
    [SerializeField] private float _partOfDamage;
    [SerializeField] private float _frequency;

    protected override void HitTarget(Enemy enemyHealth)
    {
        base.HitTarget(enemyHealth);
        enemyHealth.SetCondition(Enemy.EnemyCondition.fire, _actionTime, bulletDamage * _partOfDamage / 100, _frequency);
    }
}