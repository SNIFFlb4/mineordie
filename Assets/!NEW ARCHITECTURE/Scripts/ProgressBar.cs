﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public float progressTime;
    public Action callback;

    [SerializeField]
    private Image _barImage;

    private float _currentTime;
    private bool _isTick;

    private void Start()
    {
        _currentTime = 0;
        StartCoroutine(Timer());
    }

    private IEnumerator Timer()
    {
        _isTick = true;
        yield return new WaitForSeconds(progressTime);
        callback?.Invoke();
        yield return new WaitForEndOfFrame();
        Destroy(gameObject);
    }

    private void Update()
    {
        if (_isTick is false)
            return;

        _currentTime += Time.deltaTime;

        float barAmount = _currentTime / progressTime;

        _barImage.fillAmount = barAmount;
    }
}
