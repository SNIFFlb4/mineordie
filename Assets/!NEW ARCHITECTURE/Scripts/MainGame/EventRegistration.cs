﻿using System;

public class EventRegistration
{
    private event Action Action = delegate { };

    public void Invoke() => Action.Invoke();
    public void RemoveListener(Action listener) => Action -= listener;
    public void AddListener (Action listener)
    {
        Action -= listener;
        Action += listener;
    } 
}