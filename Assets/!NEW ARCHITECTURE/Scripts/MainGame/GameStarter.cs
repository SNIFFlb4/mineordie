﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

//Разобраться с этой ерундой и проводить инстанс проще
class GameStarter : MonoBehaviour
{
    private static GameStarter _instance;

    [SerializeField] private Player _player;
    [SerializeField] private MainCastle _mainCastle;
    [SerializeField] private LevelConfigurator _levelConfigurator;
    [SerializeField] private EnemySpawner _enemySpawner;

    private IInitialized Character;
    private IInitialized CastleBase;
    private IInitialized EnemySpawner;
    private IInitialized LevelConfigurator;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

        this.Character = _player?.GetComponent<IInitialized>();
        this.CastleBase = _mainCastle?.GetComponent<IInitialized>();
        this.LevelConfigurator = _levelConfigurator?.GetComponent<IInitialized>();
        this.EnemySpawner = _enemySpawner?.GetComponent<IInitialized>();

        Character?.Init();
        CastleBase?.Init();
        LevelConfigurator?.Init();
        EnemySpawner?.Init();

        Events.OnGameStarted?.Invoke();
    }
}