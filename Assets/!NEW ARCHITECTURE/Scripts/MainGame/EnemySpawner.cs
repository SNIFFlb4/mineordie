﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

enum EnemyType
{
    Octopus,
    OctopusTentackle,
    Bomb,
    Spider,
    AirShark
}

class EnemySpawner : MonoBehaviour, IInitialized
{
    #region Need to test spawn concrete Enemy
    [Header("Enemy need to spawn")]
    [SerializeField] private EnemyType enemyType;
    private Dictionary<EnemyType, Enemy> enemiesPrefabsMap;
    #endregion

    [Header("Prefabs")]
    [SerializeField] private Enemy OctopusEnemy;
    [SerializeField] private Enemy BombEnemy;
    [SerializeField] private Enemy Spider;
    [SerializeField] private Enemy OctopusTentacle;
    [SerializeField] private Enemy AirShark;

    [Header("MainHouse")]
    [SerializeField] MainCastle mainHouse;

    [Header("Test spawn queue")]
    [SerializeField] private EnemyType[] _enemies;
    [SerializeField] private float _timeBetweenSpawn;

    public bool IsInitialized { get; private set; }

    private List<SpawnPoint> spawnPoints;
    private Dictionary<EnemyType, Pool<Enemy>> enemyPoolMap;

    private void Awake()
    {
        //Убрать вызов в класс выше по иерархии
        Init();
    }

    public void Init()
    {
        spawnPoints = new List<SpawnPoint>();
        spawnPoints = GetComponentsInChildren<SpawnPoint>().ToList();



        #region Need to test spawn concrete Enemy
        enemiesPrefabsMap = new Dictionary<EnemyType, Enemy>();
        enemiesPrefabsMap[EnemyType.Octopus] = OctopusEnemy;
        enemiesPrefabsMap[EnemyType.OctopusTentackle] = OctopusTentacle;
        enemiesPrefabsMap[EnemyType.Bomb] = BombEnemy;
        enemiesPrefabsMap[EnemyType.Spider] = Spider;
        enemiesPrefabsMap[EnemyType.AirShark] = AirShark;
        #endregion



        enemyPoolMap = new Dictionary<EnemyType, Pool<Enemy>>();
        enemyPoolMap[EnemyType.Octopus] = new Pool<Enemy>(3, OctopusEnemy, true);
        enemyPoolMap[EnemyType.OctopusTentackle] = new Pool<Enemy>(3, OctopusTentacle, true);
        enemyPoolMap[EnemyType.AirShark] = new Pool<Enemy>(3, AirShark, true);
        enemyPoolMap[EnemyType.Spider] = new Pool<Enemy>(3, Spider, true);
        enemyPoolMap[EnemyType.Bomb] = new Pool<Enemy>(3, BombEnemy, true);


        IsInitialized = true;

        StartCoroutine(TestSpawnEnemyQueue());
    }

    private IEnumerator TestSpawnEnemyQueue()
    {
        for (int i = 0; i < _enemies.Length; i++)
        {
            yield return new WaitForSeconds(_timeBetweenSpawn);
            SpawnEnemy(_enemies[i]);
        }
    }

    #region Need to test spawn concrete Enemy
    [ContextMenu("Spawn Selected Enemy")]
    public void SpawnEnemy() 
        => SpawnEnemy(enemyType);
    #endregion

    private void SpawnEnemy(EnemyType enemyType, Transform spawnPlace = null)
    {
        if (spawnPlace == null)
            spawnPlace = spawnPoints[Random.Range(0, spawnPoints.Count)].transform;

        var spawnedEnemy = enemyPoolMap[enemyType].GetObject();
        //spawnedEnemy.GetComponent<NavMeshAgent>().Warp(new Vector3(0, 0, 0));


        spawnedEnemy.Init();
        // Надо исправить
        spawnedEnemy.GetComponent<NavMeshAgent>().Warp(spawnPlace.position);
        spawnedEnemy.SetTarget(mainHouse);


        if (enemyType.Equals(EnemyType.Octopus))
        {
            spawnedEnemy.Health.OnPersonDead += OnOctopusDead;
        }
    }

    private void OnOctopusDead(object sender)
    {
        var octopusTransform = ((HealthComponent)sender)?.Parent.transform;
        octopusTransform.position += new Vector3(0, 1, 0);
        
        for (int i = 0; i < 8; i++)
        {
            SpawnEnemy(EnemyType.OctopusTentackle, octopusTransform);
        }
    }
}