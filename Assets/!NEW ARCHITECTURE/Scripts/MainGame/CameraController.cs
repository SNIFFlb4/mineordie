﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class CameraController : MonoBehaviour
{
    public float frequency = 5;
    public float amplitude = 5;
    public float time = 5;

    [ContextMenu("Test noise")]
    public void TestCameraShake() =>
    SetSmoothlyNoise(frequency, amplitude, time);


    //Пока синглтон. Но стоит подуматЬ, как от него избавиться
    public static CameraController Instance;

    [SerializeField] private CinemachineVirtualCamera _virtualCamera;

    private CinemachineBasicMultiChannelPerlin _noiser;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
            Destroy(this.gameObject);


        _noiser = _virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        if (_noiser == null)
            Debug.LogError("There is no Noise in Virtual Camera or there is no VirtualCamera");
    }

    public void SetNoise (float frequency, float amplitude)
    {
        _noiser.m_FrequencyGain = frequency;
        _noiser.m_AmplitudeGain = amplitude;
    }

    public void StopNoise ()
    {
        SetNoise(0, 0);
    }


    public void SetSmoothlyNoise (float frequency, float amplitude, float time)
    {
        StartCoroutine(SmoothNoise(frequency, amplitude, time));
    }

    private IEnumerator SmoothNoise (float frequency, float amplitude, float time)
    {
        float currentTime = 0;

        while (frequency>0 && amplitude>0)
        {
            SetNoise(frequency, amplitude);

            currentTime += Time.deltaTime;

            frequency = Mathf.SmoothStep(frequency, 0, currentTime/time);
            amplitude = Mathf.SmoothStep(amplitude, 0, currentTime/time);

            yield return null;
        }
        StopNoise();
    }
}