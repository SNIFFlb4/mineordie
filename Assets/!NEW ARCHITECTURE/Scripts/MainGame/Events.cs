﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Events
{
    public static readonly EventRegistration OnGameStarted = new EventRegistration();

    public static readonly EventRegistration OnGameOver = new EventRegistration();

    public static readonly EventRegistration OnRoundStarted = new EventRegistration();

    public static readonly EventRegistration OnRoundCompleted = new EventRegistration();
}