﻿using UnityEngine;

class InputController
{
    private Vector2 _startTouch;

    public Vector2 GetInput()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];

            if (touch.phase == TouchPhase.Began)
                _startTouch = touch.position;

            Vector2 delta = touch.position - _startTouch;

            return delta.normalized;
        }
        else
        {
            return Vector2.zero;
        }
    }
}