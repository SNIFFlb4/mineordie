﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class LevelConfigurator : MonoBehaviour, IInitialized
{
    public bool IsInitialized { get; private set; }

    public void Init()
    {
        Debug.Log("LevelConfigurator is initialized");
        IsInitialized = true;
    }
}