﻿using UnityEngine;

class MainCastle : MonoBehaviour
{
    [SerializeField] private int maxHealth = 1000;

    public HealthComponent Health { get; private set; }

    private void Awake()
    {
        Init();
    }

    public void Init()
    {
        Health = new HealthComponent(maxHealth);
        Health.OnPersonDead += OnCastleDestroyed;
    }

    private void OnCastleDestroyed(object sender)
    {
        Events.OnGameOver?.Invoke();
    }
}