﻿using System.Collections.Generic;
using UnityEngine;

public class Pool<T> where T : MonoBehaviour, IPoolObject
{
    public T prefab { get; private set; }
    public bool autoExpand { get; private set; }

    private static Transform mainContainer;
    private Transform _poolContainer;
    private List<T> _pool;

    public Pool (int count, T prefab, bool autoExpand)
    {
        this.prefab = prefab;
        this.autoExpand = autoExpand;

        CreateContainer();
        CreatePool(count);
    }

    private void CreateContainer()
    {
        if (mainContainer == null)
        {
            GameObject goContainer = new GameObject("==Pool Container==");
            mainContainer = goContainer.transform;
        }
        string containerName = $"{typeof(T).Name}_{prefab.name}";
        _poolContainer = new GameObject(containerName).transform;
        _poolContainer.SetParent(mainContainer);
    }

    public T GetObject ()
    {
        if (HasFreeElement(out var element))
            return element;

        if (autoExpand)
            return CreateObject();

        throw new System.Exception($"There is no free element in pool of type {typeof(T)} or pool is not auto expand");
    }

    private bool HasFreeElement (out T element)
    {
        foreach (var item in _pool)
        {
            if (item.gameObject.activeInHierarchy is false)
            {
                element = item;
                element.gameObject.SetActive(true);
                return true;
            }
        }
        element = null;
        return false;
    }

    private void CreatePool(int count)
    {
        _pool = new List<T>();

        for (int i = 0; i < count; i++)
        {
            CreateObject();
        }
    }

    private T CreateObject(bool preActivated = false)
    {
        T createdObject = Object.Instantiate(this.prefab, _poolContainer);
        createdObject.gameObject.SetActive(preActivated);
        createdObject.name = createdObject.name + createdObject.GetInstanceID();
        createdObject.OnObjectNeededToDeactivate += OnObjectDeactivated;
        _pool.Add(createdObject);
        return createdObject;
    }

    private void OnObjectDeactivated(IPoolObject obj)
    {
        obj.ResetBeforeBackToPool();
    }
}