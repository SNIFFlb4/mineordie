﻿using UnityEngine;

class WeaponUI : MonoBehaviour
{
    [SerializeField]
    private GameObject _UI;

    public void ShowWeaponUI()
    {
        _UI.SetActive(true);
    }

    public void HideWeaponUI()
    {
        _UI.SetActive(false);
    }
}
