﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HpBarUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private Image _hpBar;
    [SerializeField] private GameObject _allObj;

    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
    }

    private void LateUpdate()
    {
        CameraLook();
    }

    public void ShowHealthUI(object sender, int currentHp, int maxHp)
    {
        _allObj.SetActive(true);
        float hpInPer = (float)currentHp / (float)maxHp;
        _hpBar.fillAmount = hpInPer;
    }

    public void ShowLevelUI(object sender, int level)
    {
        _levelText.text = level.ToString();
    }

    private void CameraLook()
    {
        Vector3 lookHere = new Vector3(transform.position.x, _camera.transform.position.y, _camera.transform.position.z);
        transform.LookAt(lookHere);
    }
}
