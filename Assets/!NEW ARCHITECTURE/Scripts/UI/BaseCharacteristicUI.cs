﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BaseCharacteristicUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _healthText;
    [SerializeField] private TMP_Text _oilText;
    [SerializeField] private TMP_Text _ironText;

    [SerializeField] private MainCastle _mainCastle;
    [SerializeField] private Player _player;

    private void Start()
    {
        _mainCastle.Health.OnHealthChanged += SetHealthText;
        _player.PlayerResources.OnResourcesChanged += SetResourcesText;

        SetHealthText(_mainCastle, _mainCastle.Health.Hp, 0);
    }

    private void OnEnable()
    {
        
    }

    private void OnDisable()
    {
        _mainCastle.Health.OnHealthChanged -= SetHealthText;
        _player.PlayerResources.OnResourcesChanged -= SetResourcesText;
    }

    public void SetHealthText(object sender, int currentHp, int maxhp)
    {
        _healthText.text = currentHp.ToString();
    }    

    public void SetResourcesText(int oil, int iron)
    {
        _oilText.text = "Oil: " + oil.ToString();
        _ironText.text = "Iron: " + iron.ToString();
    }
}
