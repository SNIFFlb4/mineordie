﻿using System;
using System.Collections.Generic;

    public enum ResourceType {
        Iron,
        Oil
    }

class CharacterResources : IInitialized
{
    public event Action<int, int> OnResourcesChanged;

    private Dictionary<ResourceType, int> ResourceMap;
    public bool IsInitialized { get; private set; }

    public CharacterResources(int startIronCount, int startOilCount)
    {
        ResourceMap = new Dictionary<ResourceType, int>();

        ResourceMap[ResourceType.Iron] = startIronCount;
        ResourceMap[ResourceType.Oil] = startOilCount;
    }

    public void Init()
    {
        OnResourcesChanged?.Invoke(ResourceMap[ResourceType.Oil], ResourceMap[ResourceType.Iron]);

        IsInitialized = true;
    }

    public bool HaveEnoughtResource(ResourceType type, int count)
    {
        if (ResourceMap.ContainsKey(type))
            return ResourceMap[type] >= count;
        else
            return false;
    }

    public void AddResource(ResourceType type, int count)
    {
        if (ResourceMap.ContainsKey(type))
            ResourceMap[type] += count;
        else
            ResourceMap[type] = count;
        OnResourcesChanged?.Invoke(ResourceMap[ResourceType.Oil], ResourceMap[ResourceType.Iron]);
    }

    public void SpendResource(ResourceType type, int count)
    {
        if (ResourceMap.ContainsKey(type))
            ResourceMap[type] -= count;
        else
            throw new Exception($"There is no resource: {type}");
        OnResourcesChanged?.Invoke(ResourceMap[ResourceType.Oil], ResourceMap[ResourceType.Iron]);
    }
}
