﻿using UnityEngine;

class ReloadPlatform : Platform
{
    //CONCRETE PLATFORM PARAMETERS
    private float _timeForReloadWeapon;
    private ResourceType _resourcesForReloadType;
    private int _resourcesForReloadCount;
    private int _addedAmmo;


    protected override void ChooseOperation()
    {
        if (player.IsWeaponInHands)
        {
            _timeForReloadWeapon = player.WeaponInHands.Config.timeForReload;
            _resourcesForReloadType = player.WeaponInHands.Config.resourceForReloadType;
            _resourcesForReloadCount = player.WeaponInHands.Config.resourcesForReloadCount;
            _addedAmmo = player.WeaponInHands.Config.bulletsAddedAfterReload;

            if (resources.HaveEnoughtResource(_resourcesForReloadType, _resourcesForReloadCount))
            {
                if (weaponOnPlatform is null)
                {
                    //В руках оружие, на платформе пусто
                    StartProgressBar(timeToTakeOrPutWeapon, PutWeaponAndStartOperation);
                }
                else
                    //В руках оружие, на платформе тоже
                    StartProgressBar(timeToTakeOrPutWeapon, SwapWeaponsAndStartOperation);
            }
        }
        else
        {
            if (weaponOnPlatform != null)
                //В руках пусто, на платформе есть оружие
                StartProgressBar(timeToTakeOrPutWeapon, TakeWeaponFromPlatform);
        }
    }

    protected override void StartConcreteOperation()
    {
        StartProgressBar(_timeForReloadWeapon, ReloadWeapon);
        resources.SpendResource(_resourcesForReloadType, _resourcesForReloadCount);
        platformDoOperation = true;
        mediator?.StartOperationOnPlatform();
    }

    private void ReloadWeapon()
    {
        weaponOnPlatform.AddAmmo(_addedAmmo);
        platformDoOperation = false;
        mediator?.OperationOnPlatformStopped();
    }
}