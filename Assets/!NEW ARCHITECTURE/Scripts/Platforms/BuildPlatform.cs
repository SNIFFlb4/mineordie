﻿using UnityEngine;

class BuildPlatform : Platform
{
    //CONCRETE PLATFORM PARAMETERS
    [SerializeField]
    private Weapon _weaponBuildedInPlatform;


    private float _timeForBuildWeapon;
    private ResourceType _resourceType;
    private int _buildCoast;


    private void Start()
    {
        _timeForBuildWeapon = _weaponBuildedInPlatform.Config.timeForBuild;
        _resourceType = _weaponBuildedInPlatform.Config.resourcesType;
        _buildCoast = _weaponBuildedInPlatform.Config.resourcesForBuild;
    }

    protected override void ChooseOperation()
    {
        if (player.IsWeaponInHands)
        {
            if (weaponOnPlatform is null)
                StartProgressBar(timeToTakeOrPutWeapon, PutPlayerWeaponOnPlatform);

            else
                StartProgressBar(timeToTakeOrPutWeapon, SwapWeapons);
        }
        else
        {
            if (weaponOnPlatform is null)
            {
                if (resources.HaveEnoughtResource(_resourceType, _buildCoast))
                    StartConcreteOperation();
            }

            else
                StartProgressBar(timeToTakeOrPutWeapon, TakeWeaponFromPlatform);
        }
    }

    protected override void StartConcreteOperation()
    {
        StartProgressBar(_timeForBuildWeapon, CreateWeapon);
        resources.SpendResource(_resourceType, _buildCoast);
        platformDoOperation = true;
        mediator.StartOperationOnPlatform();
    }

    private void CreateWeapon()
    {
        Weapon tempWeapon = Instantiate(_weaponBuildedInPlatform);
        tempWeapon.SetStartCharacteristics();
        PutWeaponOnPlatform(tempWeapon);
        mediator.OperationOnPlatformStopped();
        //mediator?.WeaponBuild();
        platformDoOperation = false;
    }
}