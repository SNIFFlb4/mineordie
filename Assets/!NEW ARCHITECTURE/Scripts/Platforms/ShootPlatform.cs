﻿class ShootPlatform : Platform
{
    protected override void StartConcreteOperation()
    {
        weaponOnPlatform.TurnOn();
    }

    protected override void ChooseOperation()
    {
        if (player.IsWeaponInHands)
        {
            if (weaponOnPlatform is null)
            {
                //В руках оружие, на платформе пусто
                StartProgressBar(timeToTakeOrPutWeapon, PutWeaponAndStartOperation);
            }

            else
                //В руках оружие, на платформе тоже

                StartProgressBar(timeToTakeOrPutWeapon, SwapWeaponsAndStartOperation);
        }
        else
        {
            if (weaponOnPlatform != null)
                //В руках пусто, на платформе есть оружие
                StartProgressBar(timeToTakeOrPutWeapon, TakeWeaponFromPlatform);
        }
    }
}