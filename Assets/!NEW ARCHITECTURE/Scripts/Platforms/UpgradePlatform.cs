﻿using UnityEngine;

class UpgradePlatform : Platform
{
    //CONCRETE PLATFORM PARAMETERS
    private float _timeForUpgradeWeapon;
    private ResourceType _resourceForUpgradeType;
    private int _resourceForUpgradeCount;

    protected override void ChooseOperation()
    {
        if (player.IsWeaponInHands)
        {
            _timeForUpgradeWeapon = player.WeaponInHands.Config.timeForUpgrade;
            _resourceForUpgradeType = player.WeaponInHands.Config.resourceForUpgradeType;
            _resourceForUpgradeCount = player.WeaponInHands.Config.resourcesForUprgadeCount;

            if (resources.HaveEnoughtResource(_resourceForUpgradeType, _resourceForUpgradeCount))
            {
                if (weaponOnPlatform is null)
                {
                    //В руках оружие, на платформе пусто
                    StartProgressBar(timeToTakeOrPutWeapon, PutWeaponAndStartOperation);
                }
                else
                    //В руках оружие, на платформе тоже
                    StartProgressBar(timeToTakeOrPutWeapon, SwapWeaponsAndStartOperation);
            }
        }
        else
        {
            if (weaponOnPlatform != null)
                //В руках пусто, на платформе есть оружие
                StartProgressBar(timeToTakeOrPutWeapon, TakeWeaponFromPlatform);
        }
    }

    protected override void StartConcreteOperation()
    {
        StartProgressBar(_timeForUpgradeWeapon, UpgradeWeapon);
        resources.SpendResource(_resourceForUpgradeType, _resourceForUpgradeCount);
        platformDoOperation = true;
        mediator?.StartOperationOnPlatform();
    }

    private void UpgradeWeapon()
    {
        weaponOnPlatform.Upgrade();
        platformDoOperation = false;
        mediator?.OperationOnPlatformStopped();
    }
}