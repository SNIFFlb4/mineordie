﻿using System;
using UnityEngine;

abstract class Platform : MonoBehaviour
{
    [SerializeField]
    protected static float timeToTakeOrPutWeapon = 1f;

    [SerializeField]
    protected PlatformMediator mediator;


    //MAIN PARAMETERS
    public ProgressBar progressBarPrefab;
    public Transform placeForWeapon;


    abstract protected void StartConcreteOperation();
    abstract protected void ChooseOperation();


    protected Player player;
    protected Weapon weaponOnPlatform;
    protected bool platformDoOperation;
    protected CharacterResources resources;

    private ProgressBar _currentProgressBar;


    private void OnTriggerEnter(Collider other)
    {
        if (player == null)
        {
            if (other.TryGetComponent(out player))
                resources = player.PlayerResources;
            else return;
        }

        ChooseOperation();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.Equals(player?.gameObject))
        {
            if (_currentProgressBar != null && platformDoOperation is false)
            {
                Destroy(_currentProgressBar.gameObject);
            }
        }
    }

    protected virtual void SwapWeapons()
    {
        Weapon tempWeapon = player.WeaponInHands;

        player.TakeWeapon(weaponOnPlatform);

        PutWeaponOnPlatform(tempWeapon);
    }

    protected void TakeWeaponFromPlatform()
    {
        player.TakeWeapon(weaponOnPlatform);

        weaponOnPlatform = null;
    }

    protected void PutPlayerWeaponOnPlatform() =>
        PutWeaponOnPlatform(player.GetWeapon());

    protected void PutWeaponOnPlatform(Weapon weapon)
    {
        weaponOnPlatform = weapon;
        weaponOnPlatform.transform.SetParent(placeForWeapon);
        weaponOnPlatform.transform.position = placeForWeapon.position;
        mediator?.WeaponPutOnPlatform();
    }

    protected void StartProgressBar(float time, Action callback)
    {
        if (platformDoOperation)
            return;

        progressBarPrefab.progressTime = time;

        _currentProgressBar = Instantiate(progressBarPrefab, transform);
        _currentProgressBar.callback = callback;
        _currentProgressBar.transform.position += new Vector3(0, 0.5f, -1.5f);
    }

    protected void SwapWeaponsAndStartOperation()
    {
        SwapWeapons();
        StartConcreteOperation();
    }

    protected void PutWeaponAndStartOperation()
    {
        PutPlayerWeaponOnPlatform();
        StartConcreteOperation();
    }
}