﻿using UnityEngine;


class UtilizationPlatform : Platform
{
    //CONCRETE PLATFORM PARAMETERS
    private float _timeForUtilizeWeapon;
    private ResourceType _addedResourceType;
    private int _addedResourceCount;


    protected override void ChooseOperation()
    {
        if (player.IsWeaponInHands)
        {
            _timeForUtilizeWeapon = player.WeaponInHands.Config.timeForUtil;
            _addedResourceType = player.WeaponInHands.Config.addedResourcesType;
            _addedResourceCount = player.WeaponInHands.Config.addedResourceCount;

            if (weaponOnPlatform is null)
            {
                //В руках оружие, на платформе пусто
                StartProgressBar(timeToTakeOrPutWeapon, PutWeaponAndStartOperation);

            }

            else
                //В руках оружие, на платформе тоже
                StartProgressBar(timeToTakeOrPutWeapon, SwapWeaponsAndStartOperation);
        }
        else
        {
            if (weaponOnPlatform != null)
                //В руках пусто, на платформе есть оружие
                StartProgressBar(timeToTakeOrPutWeapon, TakeWeaponFromPlatform);
        }
    }

    protected override void StartConcreteOperation()
    {
        StartProgressBar(_timeForUtilizeWeapon, UtilizeWeapon);

        platformDoOperation = true;
        mediator?.StartOperationOnPlatform();
    }

    private void UtilizeWeapon()
    {
        Destroy(weaponOnPlatform.gameObject);
        resources.AddResource(_addedResourceType, _addedResourceCount);
        platformDoOperation = false;
        mediator?.OperationOnPlatformStopped();
    }
}