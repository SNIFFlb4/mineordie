﻿using UnityEngine;

class PlatformMediator : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    [SerializeField] private PlatformUI _ui;
    [SerializeField] private VFXSystem _effectsSystem;

    public void WeaponBuild (AudioClip audio)
    {
        //_audio.PlayOneShot();
        _effectsSystem.PlayEffect(VFXType.WeaponCreated);
    }

    public void WeaponUtilize ()
    {
        _effectsSystem.PlayEffect(VFXType.WeaponUtilize);
    }

    public void WeaponPutOnPlatform ()
    {
        _effectsSystem.PlayEffect(VFXType.WeaponPutOnPlatform);
    }

    public void StartOperationOnPlatform ()
    {
        _effectsSystem.PlayEffect(VFXType.DoingOperationOnPlatform);
    }

    public void OperationOnPlatformStopped ()
    {
        _effectsSystem.StopEffect(VFXType.DoingOperationOnPlatform);
    }
}
