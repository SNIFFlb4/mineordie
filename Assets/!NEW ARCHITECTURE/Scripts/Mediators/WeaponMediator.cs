﻿using UnityEngine;

class WeaponMediator : MonoBehaviour
{
    private const string LayerName = "OnOffLayer";
    private const string ShotTriggerName = "Shot";
    [SerializeField] private Weapon _weapon;

    [SerializeField] private AudioSource _audio;
    [SerializeField] private Animator _animator;
    [SerializeField] private WeaponUI _ui;
    [SerializeField] private VFXSystem _VFXSystem;

    private int _animatorTurnOnOffLayerIndex;

    private void Start()
    {
        _animatorTurnOnOffLayerIndex = _animator.GetLayerIndex(LayerName);
    }

    public void TurnOn()
    {
        _animator.SetLayerWeight(_animatorTurnOnOffLayerIndex, 1);
    }

    public void TurnOff()
    {
        _animator.SetLayerWeight(_animatorTurnOnOffLayerIndex, 0);
    }

    public void Shot()
    {
        _audio.PlayOneShot(_weapon.Config.shotSound);
        _VFXSystem.PlayEffect(VFXType.WeaponShot);
        _animator.SetTrigger(ShotTriggerName);
    }
}