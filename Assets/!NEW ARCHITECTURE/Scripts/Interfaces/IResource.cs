﻿interface IResource
{
    int Count { get; }

    void AddResource(int value);
    void DecreaseResource(int value);
}