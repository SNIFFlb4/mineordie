﻿interface IInitialized
{
    bool IsInitialized { get; }

    void Init();
}