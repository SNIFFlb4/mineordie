﻿using System;
using System.Collections;
using UnityEngine;

abstract class Weapon : MonoBehaviour, IUpgradable
{
    public Action<int> OnBulletCountChanged;

    [SerializeField] protected WeaponConfig config;
    [SerializeField] protected Transform bulletPlace;
    [SerializeField] protected LayerMask enemyLayer;
    [SerializeField] protected WeaponMediator mediator;

    public bool IsActivated { get; set; }
    public WeaponConfig Config => config;

    private int _bulletCount = 0;
    public int BulletCount
    {
        get => _bulletCount;
        private set
        {
            _bulletCount = Mathf.Clamp(value, 0, int.MaxValue);
            OnBulletCountChanged?.Invoke(_bulletCount);
        }
    }

    protected Enemy enemyTarget;
    protected WaitForSeconds waitBetweenShots;
    protected Pool<Bullet> bulletPool;
    protected Bullet currentBullet;

    private int _level = 1;
    private bool _canShoot = false;
    private float _timeBetweenShots;
    private bool autoExpand = true;

    public void SetStartCharacteristics()
    {
        BulletCount = config.bulletsStartCount;
        _timeBetweenShots = 60f / config.rateOffFire;
        waitBetweenShots = new WaitForSeconds(_timeBetweenShots);

        const int Count = 5;
        bulletPool = new Pool<Bullet>(Count, Config.bulletPrefab, autoExpand);
    }

    public void Upgrade()
    {
        _level++;
        Debug.Log("Weapon is updated");
    }

    public void AddAmmo(int count)
    {
        BulletCount += count;
    }

    public void TurnOn()
    {
        _canShoot = true;
        mediator.TurnOn();
        StartCoroutine(nameof(Shoot));
    }

    public void TurnOff()
    {
        _canShoot = false;
        mediator.TurnOff();
        StopCoroutine(nameof(Shoot));
    }

    private void Update()
    {
        if (_canShoot is false)
            return;

        if (enemyTarget != null)
            LooAtTarget();
    }

    private void LooAtTarget()
    {
        transform.LookAt(enemyTarget.transform);
        transform.localRotation = new Quaternion(0, transform.localRotation.y,
            transform.localRotation.z, transform.localRotation.w);
    }

    protected bool ChooseTarget()
    {
        if (enemyTarget != null && enemyTarget.gameObject.activeInHierarchy)
            return true;

        //поиск ближайшего врага
        var enemyColliders = Physics.OverlapSphere(transform.position, config.attackDistance, enemyLayer);
        if (enemyColliders.Length != 0)
        {
            FindNearestEnemy(enemyColliders);
            enemyTarget.Health.OnPersonDead += OnTargetEnemyDead;
            return true;
        }
        else
            return false;
    }

    private void OnTargetEnemyDead(object sender)
    {
        enemyTarget.Health.OnPersonDead -= OnTargetEnemyDead;
        enemyTarget = null;
    }

    private void FindNearestEnemy(Collider[] enemies)
    {
        float minDistance = float.MaxValue;
        int index = 0;

        for (int i = 0; i < enemies.Length; i++)
        {
            if (Vector3.Distance(transform.position, enemies[i].transform.position) < minDistance)
            {
                index = i;
                minDistance = Vector3.Distance(transform.position, enemies[i].transform.position);
            }
        }

        enemyTarget = enemies[index].gameObject.GetComponent<Enemy>();
        Debug.Log($"Current target : {enemyTarget?.name}");
    }

    protected IEnumerator Shoot()
    {
        if (ChooseTarget() && BulletCount > 0)
        {
            Fire();
            mediator.Shot();
        }
        yield return waitBetweenShots;
        StartCoroutine(nameof(Shoot));
    }

    protected virtual void Fire()
    {
        currentBullet = bulletPool.GetObject();
        BulletCount--;
        currentBullet.transform.position = bulletPlace.position;
        currentBullet.transform.rotation = bulletPlace.rotation;
        currentBullet.StartMove();
    }
}