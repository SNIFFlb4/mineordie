﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class CatapultTower : Weapon
{
    protected override void Fire()
    {
        base.Fire();
        
        Vector3 targetPosition = new Vector3(enemyTarget.transform.position.x, 
            enemyTarget.transform.position.y,
            enemyTarget.transform.position.z + 1);

        currentBullet.SetPositions(currentBullet.transform.position, targetPosition);
    }
}
