﻿using UnityEngine;

[CreateAssetMenu(fileName = "WeaponConfig", menuName = "Add New/Config/Weapon")]
class WeaponConfig : ScriptableObject
{
    [Header("TTX")]
    public string name;
    public float attackDistance;
    public Bullet bulletPrefab;
    public float rateOffFire;
    public float rotationSpeed;

    [Header("Sound")]
    public AudioClip shotSound;

    [Header("Build Settings")]
    public ResourceType resourcesType;
    public int resourcesForBuild;
    public float timeForBuild;
    public int bulletsStartCount;

    [Header("Reload Settings")]
    public float timeForReload;
    public ResourceType resourceForReloadType;
    public int resourcesForReloadCount;
    public int bulletsAddedAfterReload;

    [Header("Upgrade Settings")]
    public ResourceType resourceForUpgradeType;
    public int resourcesForUprgadeCount;
    public float timeForUpgrade;
    public float damageIncrement;
    public float rateOffFireIncrement;
    public float upgradeCostIncrement;

    [Header("Utilization Settings")]
    public float timeForUtil;
    public ResourceType addedResourcesType;
    public int addedResourceCount;
}
