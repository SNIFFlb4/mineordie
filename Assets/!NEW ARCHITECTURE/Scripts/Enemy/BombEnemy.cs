﻿using System.Collections;
using UnityEngine;

class BombEnemy : Enemy
{
    public VFXComponent explosionEffect;

    public override IEnumerator Attack()
    {
        Debug.Log("ATTACK");
        EnemyAnimator.ChangeAnimState(EnemyAnimState.AttackTriggered);
        yield return null;
    }

    //Called from imported animation of BomberEnemy
    private void OnAttackEnded ()
    {
        var exp = Instantiate(explosionEffect, transform.position, transform.rotation);
        Destroy(exp.gameObject, 2f);

        CameraController.Instance.SetSmoothlyNoise(0.7f, 0.7f, 1f);

        _castleTarget?.Health.TakeDamage(Config.damage);
        ReturnToPool();
    }
}