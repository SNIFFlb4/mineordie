﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class DividableEnemy : Enemy
{
    [Header("Class characteristic")]
    [SerializeField] private PartOfDividEnemy _childrenPrefab;
    [SerializeField] private int childrenCount;

    //protected override void Dead(HealthComponent deadHealth)
    //{
    //    InstantiateChildren();
    //    base.Dead(this.Health);
    //}

    //private void InstantiateChildren()
    //{
    //    for (int i = 0; i < childrenCount; i++)
    //    {
    //        PartOfDividEnemy child = Instantiate(_childrenPrefab, transform);
    //        child.Init();
    //        child.SetTarget(_castleTarget);
    //        child.transform.SetParent(null);
    //    }
    //}

    //Called from imported animation on OctopusEnemy
    private void ShakeCamera ()
    {
        CameraController.Instance.SetSmoothlyNoise(0.5f, 0.5f, 0.5f);
    }
}