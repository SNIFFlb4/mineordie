﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Add New/Config/Enemy")]
class EnemyConfig : ScriptableObject
{
    public string name;
    public int hp;
    public float moveSpeed;
    public int damage;
    public float timeBetweenAttack;
    public int rewardOnDeath;

    public int hpUpgrade;
    public float damageUpgrade;
    public int rewardOnDeathUpgrade;
}