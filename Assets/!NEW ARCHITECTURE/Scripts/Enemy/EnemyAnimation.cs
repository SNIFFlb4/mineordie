﻿using UnityEngine;

public enum EnemyAnimState
{
    IDLE,
    Walk, 
    AttackBool,
    AttackTriggered,
    Death
}

class EnemyAnimation : IInitialized
{
    private const string BOOL_IS_WALK = "isWalking";
    private const string BOOL_IS_ATTACK = "isAttacking";
    private const string ATTACK_TRIGGER = "attack";
    private const string DEATH_TRIGGER = "death";

    private Animator _animator;
    private EnemyAnimState _currentState;

    public bool IsInitialized { get; private set; }

    public EnemyAnimation (Animator animator)
    {
        _animator = animator;
    }

    public void Init()
    {
        ChangeAnimState(EnemyAnimState.IDLE);

        IsInitialized = true;
    }

    public void ChangeAnimState (EnemyAnimState newState)
    {
        if (_currentState == newState)
            return;

        _currentState = newState;

        switch (_currentState)
        {
            case EnemyAnimState.IDLE:
                _animator.SetBool(BOOL_IS_WALK, false);
                break;
            case EnemyAnimState.Walk:
                _animator.SetBool(BOOL_IS_WALK, true);
                break;
            case EnemyAnimState.AttackBool:
                Debug.Log("ATTACK BOOLed");
                _animator.SetBool(BOOL_IS_ATTACK, true);
                break;
            case EnemyAnimState.AttackTriggered:
                Debug.Log("ATTACK TRIGGERED");
                _animator.SetTrigger(ATTACK_TRIGGER);
                break;
            case EnemyAnimState.Death:
                _animator.SetTrigger(DEATH_TRIGGER);
                break;
            default:
                Debug.LogError("Unknown enemy state");
                break;
        }
    }
}