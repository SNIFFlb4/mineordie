﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
class Enemy : MonoBehaviour, IUpgradable, IInitialized, IPoolObject
{
    public event Action<object, int> OnLevelChanged;
    public event Action<IPoolObject> OnObjectNeededToDeactivate;

    [Header("Base characteristic")]
    [SerializeField] private EnemyConfig _config;
    [SerializeField] private Animator _animator;
    [SerializeField] private HpBarUI _uiBar;

    private NavMeshAgent _agent;

    public EnemyConfig Config => _config;
    public int Level { get; private set; }
    public bool IsInitialized { get; private set; }
    public HealthComponent Health { get; protected set; }
    public EnemyAnimation EnemyAnimator { get; private set; }
    public MainCastle _castleTarget { get; protected set; }

    public enum EnemyCondition
    {
        none,
        freeze,
        fire
    };

    public EnemyCondition Conditions { get; protected set; } = EnemyCondition.none;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GlobalVars.CASTLE_TAG))
        {
            StartCoroutine(nameof(Attack));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        StopAllCoroutines();
    }

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    public void Init()
    {
        print(gameObject.name + " " + _agent.isOnNavMesh);

        gameObject.SetActive(true);
        _agent.isStopped = false;
        _agent.speed = Config.moveSpeed;
        
        Health = new HealthComponent(Config.hp, gameObject);
        Health.OnHealthChanged += _uiBar.ShowHealthUI;
        Health.OnPersonDead += Dead;
        _uiBar.ShowHealthUI(this, Health.Hp, Health.MaxHP);

        EnemyAnimator = new EnemyAnimation(_animator);
        EnemyAnimator.Init();
        EnemyAnimator.ChangeAnimState(EnemyAnimState.Walk);

        OnLevelChanged += _uiBar.ShowLevelUI;

        IsInitialized = true;
    }

    public void SetTarget(MainCastle castle)
    {
        _castleTarget = castle;

        float castleWidth = castle.GetComponent<BoxCollider>().bounds.size.x / 2;
        Vector3 randomInCastlePosition = castle.transform.position + 
            new Vector3(UnityEngine.Random.Range(-castleWidth, castleWidth), 0, 0);

        _agent.SetDestination(randomInCastlePosition);
    }

    protected virtual void Dead(object sender)
    {
        EnemyAnimator.ChangeAnimState(EnemyAnimState.Death);

        Health.OnPersonDead -= Dead;
        Health.OnHealthChanged -= _uiBar.ShowHealthUI;
        OnLevelChanged -= _uiBar.ShowLevelUI;

        IsInitialized = false;

        ReturnToPool();
    }

    protected void ReturnToPool ()
    {
        OnObjectNeededToDeactivate?.Invoke(this);
    }

    public void Upgrade()
    {
        Level++;
        OnLevelChanged?.Invoke(this, Level);
        Debug.Log("Enemy is upgraded");
    }

    public virtual IEnumerator Attack ()
    {
        EnemyAnimator.ChangeAnimState(EnemyAnimState.AttackBool);

        print("Attack");
        _castleTarget?.Health.TakeDamage(Config.damage);
        yield return new WaitForSeconds(Config.timeBetweenAttack);
        StartCoroutine(nameof(Attack));
    }

    // action time - Effect duration, force - Effect strength, effectsCd - Effect frequency
    public void SetCondition(EnemyCondition cond, float actionTime = 0, float force = 0, float effectsCd = 0)
    {
        switch(cond)
        {
            case EnemyCondition.freeze:
                Conditions = EnemyCondition.freeze;
                print("By freeze");
                StartCoroutine(ByFreeze(actionTime, force));
                break;

            case EnemyCondition.fire:
                Conditions = EnemyCondition.fire;
                print("By fire");
                StartCoroutine(ByFire(actionTime, force, effectsCd));
                break;

            case EnemyCondition.none:
                Conditions = EnemyCondition.none;
                print("No effects");
                ReturnDefoultCharacteristic();
                break;

            default:
                Debug.LogError("Warning in effects");
                break;
        }
    }

    protected void ReturnDefoultCharacteristic()
    {
        _agent.speed = Config.moveSpeed;
    }

    protected IEnumerator ByFreeze(float actionTime, float force)
    {
        print("cold");
        _agent.speed = Config.moveSpeed * force;
        yield return new WaitForSeconds(actionTime);
        SetCondition(EnemyCondition.none);
    }

    protected IEnumerator ByFire(float actionTime, float damage, float effectsCd)
    {
        while (actionTime > 0)
        {
            print("hot");
            Health.TakeDamage(Convert.ToInt32(damage));
            yield return new WaitForSeconds(effectsCd);
            actionTime -= Time.deltaTime + effectsCd;
        }

        SetCondition(EnemyCondition.none);
    }

    public void ResetBeforeBackToPool()
    {
        gameObject.SetActive(false);
        // Сюда прописать логику сброса 
        // характеристик врага
        // (здоровье, левел, все дела)
    }
}