﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;

public class Utils : EditorWindow
{
    private Color textColor;
    private TMP_FontAsset textFont;

    [MenuItem("Utils/Выбрать шрифт")]
    public static void ShowWindow ()
    {
        EditorWindow.GetWindow(typeof(Utils));
    }

    private void OnGUI()
    {
        GUILayout.Label("Шрифт", EditorStyles.boldLabel);
        textFont = (TMP_FontAsset)EditorGUILayout.ObjectField("", textFont, typeof(TMP_FontAsset), true);

        GUILayout.Label("Цвет текста", EditorStyles.boldLabel);
        textColor = EditorGUILayout.ColorField("Новый цвет", textColor);

        if (GUILayout.Button("Изменить"))
            ChangeTextInScene();
    }

    private void ChangeTextInScene ()
    {
        if (textFont is null)
            return;

        var textes = GameObject.FindObjectsOfType<TMP_Text>();
        foreach (var item in textes)
        {
            item.font = textFont;
            item.color = textColor;
        }
    }

    //public static void SetChoosenFontToText ()
    //{
    //    //if (Game.Instance.fontTemplate is null)
    //    //{
    //    //    Debug.LogError("Не выбран шрифт");
    //    //    return;
    //    //}

    //    var textes = GameObject.FindObjectsOfType<TMP_Text>();
    //    foreach (var item in textes)
    //    {
    //        item.font = Game.Instance.fontTemplate;
    //    }
    //}
}
#endif
