using UnityEngine;

public class ParticleDestroyInstance : MonoBehaviour
{
    public ParticleSystem ParticlePrefab;

    private ParticleSystem _destroyVFX;
    private Transform _vfxTransform;

    private Transform _transform;
    private bool _gameQuit;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    private void OnDisable()
    {
        if (_gameQuit)
            return;

        if (EffectNotExist())
            CreateDestroyEffect();

        SetNewEffectPosition();
        _destroyVFX.Play();
    }

    private bool EffectNotExist()
    {
        return _destroyVFX == null;
    }

    private void CreateDestroyEffect()
    {
        _destroyVFX = Instantiate(ParticlePrefab);
        _vfxTransform = _destroyVFX.GetComponent<Transform>();
        _vfxTransform.SetParent(transform.parent);
    }

    private void SetNewEffectPosition()
    {
        _vfxTransform.position = _transform.position;
        _vfxTransform.rotation = Quaternion.identity;
    }

    private void OnApplicationQuit() => _gameQuit = true;
}